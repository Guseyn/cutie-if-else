module.exports = {

  Else: require('./src/Else'),
  ElseIf: require('./src/ElseIf'),
  If: require('./src/If')

}
